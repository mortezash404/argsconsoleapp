﻿using System;

namespace ArgsConsoleApp
{
    public class Program
    {
        static void Main(string[] args)
        {
            foreach (var arg in args)
            {
                Console.WriteLine($"{arg} ^2 : {int.Parse(arg) ^ 2}");
            }

            Console.ReadKey();
			
			//Console.WriteLine($"Hello {args[0]}");
        }
    }
}
